import React from 'react'
import { createRoot } from 'react-dom/client'
import App from './App.tsx'
import './css/index.css'

const root = createRoot(document.getElementById("root") as HTMLElement);
root.render(<App />);

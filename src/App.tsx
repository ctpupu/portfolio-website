import React from 'react'
import NavButton from './components/navbutton.tsx'
import Section from './components/section.tsx'
import TypeWriter from './components/typewriter.tsx'
import WorkExpBlock from './components/workexp.tsx'
import Project from './components/project.tsx'
import TechStackIcon  from './components/techstack_icon.tsx'

import { TechStackData } from './data/techstack_data.ts'
import { WorkExpData, IWorkExpData } from './data/workexp_data.ts'
import { ProjectsData, IProjectsData } from './data/projects_data.ts'

import LinkedInLogo from './assets/LinkedIn_Logo.svg.png'
import ClipboardIcon from './assets/clipboard.png'
import MyPicture from './assets/my_picture.jpg'
import './css/App.css'

class App extends React.Component {
  componentDidMount() {
      const observerCallback = (entries, observer) => {
        entries.forEach(entry => {
          // Check if the element is intersecting
          if (entry.isIntersecting) {
            // Perform any action, such as adding a class or starting an animation
            entry.target.classList.add('fadein_animation');
            
            // Optional: Unobserve the element after it becomes visible
            observer.unobserve(entry.target);
          }
        });
      };
    
      // Create an Intersection Observer instance
      const observer = new IntersectionObserver(observerCallback);
    
      // Attach the observer to each element with the 'observe' class
      document.querySelectorAll('.observe').forEach(element => {
        observer.observe(element);
      });
  }
  render() {
    const totalExperiences = new Date().getFullYear() - 2010;
    return (
      <>
        <div className="topnav">
          <div>Tae Jin Chung</div>
          <div>
            <ul>
              <NavButton section_id="home">Home</NavButton>
              <NavButton section_id="work_exp">Work Experiences</NavButton>
              <NavButton section_id="projects">Projects</NavButton>
              <NavButton section_id="aboutme">About Me</NavButton>
              <NavButton section_id="contact">Contact</NavButton>
            </ul>
          </div>
        </div>
        
        <Section section_id="home">
          <h1>Tae Jin Chung</h1>
          <h2>Software Engineer/Unity Software Engineer/Web Developer</h2>
          <p>I can do <TypeWriter typeList={TechStackData} /> </p>
          <div id="tech-stack-list">
            {TechStackData.map((data:string)=><TechStackIcon tsname={data} key={data} />)}
          </div>
          <button>
            <a href="https://www.linkedin.com/in/tae-jin-chung-09b678a/" target='_blank'><img src={LinkedInLogo} alt="LinkedIn" width = "100" height="30" /></a>
          </button>
        </Section>
        
        <Section section_id="work_exp">
          <h1>Work Experiences</h1>
          {WorkExpData.map((workExpData: IWorkExpData)=><WorkExpBlock key={workExpData.timeline} {...workExpData} />)}
        </Section>

        <Section section_id="projects">
          <h1>Projects</h1>
          <div className="projects-container">
            {ProjectsData.map((data: IProjectsData)=><Project key={data.title} {...data} />)}
          </div>
        </Section>

        <Section section_id="aboutme">
          <h1>About Me</h1>
          <div className="aboutme-container">
            <span>
              <img src={MyPicture} alt="Image of Me" width="400" height="350" />
            </span>
            <span>
              <p>My name is Tae jin Chung, and I am a Software Engineer based in Los Angeles, CA, with over {totalExperiences} years of experience in Software Development.</p>
              <p>I possess a profound passion for programming, a keen aptitude for problem-solving, and a genuine love for gaming.</p>
              <p>I pride myself on being an adaptable team player, known for my versatility and reliability.</p>
              <p>I have an insatiable curiosity for acquiring new knowledge and am always ready to embrace the challenges that come with venturing into unfamiliar territory.</p>
            </span>
          </div>
        </Section>

        <Section section_id="contact">
          <h1>Contact</h1>
          <div className="contact-email">
            <span id="email-address">ctaejin@hotmail.com</span>
            <img id="clipboard-icon" src={ClipboardIcon} alt="Copy to Clipboard" width="20" height="20" onClick={(e)=>{
              navigator.clipboard.writeText("ctaejin@hotmail.com")
                .then(() => {
                  alert("Email Address copied to clipboard!");
                })
                .catch(err => {
                  console.error("Error copying text:", err);
                })
            }} />
          </div>
          <div className="contact-linkedin">
              <a href="https://www.linkedin.com/in/tae-jin-chung-09b678a/" target='_blank'><img src={LinkedInLogo} alt="LinkedIn" width = "100" height="30" /></a>
          </div>
        </Section>
      </>
    )
  }
}

export default App

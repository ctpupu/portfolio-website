import React, { useState, useEffect } from 'react'

interface TypeWriterProps {
    typeList: string[];
    typeSpeed?: number;
    backspaceSpeed?: number;
    delayOnComplete?: number;
}

interface TypeWriterStateData {
    listIndex: number;
    strIndex: number;
    direction: number;
}

export default function TypeWriter({typeList, typeSpeed = 250, backspaceSpeed = 150, delayOnComplete = 1000} : TypeWriterProps) {
    const [currentState, updateState] = useState<TypeWriterStateData>({listIndex:0, strIndex:0, direction:1})
    
    const timeDelay:number = currentState.direction === 1 ? typeSpeed : backspaceSpeed;
    const displayText:string = typeList[currentState.listIndex].slice(0, currentState.strIndex);
    useEffect(()=> {
        const timer:number = setTimeout(()=> {
            let newState:TypeWriterStateData = {...currentState};
            let delay:number = 0;
            if(newState.direction > 0) {
                if(typeList[newState.listIndex].length <= newState.strIndex) {
                    newState.direction = -1;  
                    delay = delayOnComplete; 
                }
            } else {
                if(newState.strIndex <= 0) {
                    newState.listIndex++;
                    if(newState.listIndex >= typeList.length) {
                        newState.listIndex = 0;
                    }
                    newState.direction = 1;
                    newState.strIndex = 0;
                }
            }
            newState.strIndex += newState.direction;
            setTimeout(()=>updateState(newState), delay);
        }, timeDelay);

        return () => clearTimeout(timer);
    }, [currentState]);
    return (
        <span className="type-writer">
            {displayText}
        </span>
    );
}
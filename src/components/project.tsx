import TechStackIcon from './techstack_icon.tsx'
import Link from './link.tsx'
import '../css/project.css'
import React from 'react'
import {IProjectsData, ILinks} from '../data/projects_data.ts'


export default function Project({title, image, desc, techstacks, links}:IProjectsData) {
    return (
        <div className="project-container">
            {image && <img src={image} alt="demo image" />}
            <h2>{title}</h2>
            {desc.split('\n').map((d)=><p>{d}</p>)}
            <div>
                <h3>Tech Stacks</h3>
                {techstacks.map((ts:string) => <TechStackIcon tsname={ts} key={ts} />)}
            </div>
            <div>
                <h3>Links</h3>
                {links.map((link:ILinks)=><Link key={link.name} {...link} />)}
            </div>
        </div>
    )
}
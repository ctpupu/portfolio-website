import '../css/techstack_icon.css'
import React from 'react'

interface TechStackIconProps {
    tsname: string
}

export default function TechStackIcon({tsname}:TechStackIconProps) {
    return (
        <span className="techstack-icon">{tsname}</span>
    )
}
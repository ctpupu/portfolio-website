import TechStackIcon from "./techstack_icon.tsx"
import {ExpandIcon, ExpandIconHandle} from "./expand_icon.tsx"
import '../css/workexp.css'
import React, {useRef} from "react"

interface IWorkExpBlockProps {
    company: string,
    jobtitle: string,
    timeline: string,
    desc: string,
    techstacks: string[]
}

export default function WorkExpBlock({company, jobtitle, timeline, desc, techstacks}:IWorkExpBlockProps) {
    const workexp_desc_ref = useRef<HTMLDivElement>(null);
    const expand_icon_ref = useRef<ExpandIconHandle>(null);

    let expanded = false;
    function handleExpandButton(e) {
        expanded = !expanded;

        if(workexp_desc_ref && expand_icon_ref) {
            if(expanded) {
                if(workexp_desc_ref.current != undefined) {
                    workexp_desc_ref.current?.classList.remove("workexp-collapse");
                    workexp_desc_ref.current?.classList.add("workexp-expand");
                }

                if(expand_icon_ref.current != undefined) {
                    expand_icon_ref.current.changeStyle({
                        transform: "scale(1,-1)"
                    })
                }

            } else {
                if(workexp_desc_ref.current != undefined) {
                    workexp_desc_ref.current.classList.remove("workexp-expand");
                    workexp_desc_ref.current.style.animation = "";
                    workexp_desc_ref.current.classList.add("workexp-collapse");
                }

                if(expand_icon_ref.current != undefined) {
                    expand_icon_ref.current.changeStyle({
                        transform: "scale(1,1)"
                    })
                }
            }
        }
    }
    
    return (
        <div className="workexp-container">
            <div className="workexp-header" onClick={handleExpandButton}>
                <span>
                    <span id="jobtitle">{jobtitle}</span>
                    <span id="company">{company}</span>
                    <span id="timeline">{timeline}</span>
                </span>
                <ExpandIcon id="expand_icon" ref={expand_icon_ref} width={20} height={20} />
            </div>
            <div ref={workexp_desc_ref} className="workexp-default">
                <span>{desc}</span>
                <div className="workexp-techstack-container">
                    {techstacks.map((ts)=><TechStackIcon key={ts} tsname={ts} />)}
                </div>
            </div>
        </div>
    )
}
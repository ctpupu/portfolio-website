import React from 'react'

interface NavButtonProps {
    section_id:string;
    children:React.ReactNode;
}

export default function NavButton({children, section_id}) {
    const sec_id:string = "#" + section_id;
    return (<>
        <li>
            <a href={sec_id}>{children}</a>
        </li>
    </>);
}
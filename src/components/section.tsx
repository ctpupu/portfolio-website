import React from 'react'

interface SectionProps {
    section_id:string;
    children: React.ReactNode;
}

export default function Section({children, section_id}) {
   
    return (<>
        <div id={section_id} className="section observe" >
            {children}
        </div>
    </>)
}
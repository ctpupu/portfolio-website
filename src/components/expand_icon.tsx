import React, {forwardRef, useImperativeHandle, useRef} from 'react';

export interface ExpandIconProps {
    id: string;
    width: number;
    height: number;
    ref: React.RefObject<SVGSVGElement>;
}

export interface ExpandIconHandle {
    changeStyle: (newStyle: React.CSSProperties) => void;
}

export const ExpandIcon = forwardRef<ExpandIconHandle, ExpandIconProps>((props, ref) => {
    const svgRef = useRef<SVGSVGElement>(null);

    const {id, width, height} = props;
    const polylinePoints = Math.floor(width*0.9)+","+ Math.floor(height*0.7) + " " + 
                            Math.floor(width*0.5) + "," + Math.floor(height*0.3) + " " +
                            Math.floor(width*0.1) + "," + Math.floor(height*0.7);   

    useImperativeHandle(ref, () => (
    {
        changeStyle: (newStyle: React.CSSProperties) => {
            if(svgRef.current) {
                Object.assign(svgRef.current.style, newStyle);
            }
        },
    }));
    return (
        <svg id={id} ref={svgRef} width={width} height={height} strokeLinecap="round" strokeLinejoin="round" xmlns="http://www.w3.org/2000/svg">
            <polyline points={polylinePoints} fill="none" stroke="black" strokeWidth="2" />
        </svg>
    );
});
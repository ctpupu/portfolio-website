import '../css/link.css'
import React from 'react'

interface ILinkProps {
    name: string,
    link: string
}

export default function Link({name, link} : ILinkProps) {
    return (
        <a className="link" href={link} target="_blank">{name}</a>
    );
}
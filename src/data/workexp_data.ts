export interface IWorkExpData {
    company: string;
    jobtitle: string;
    timeline: string;
    desc: string;
    techstacks: string[]
}

export const WorkExpData:IWorkExpData[] = [
    {
        company: "Age of Learning",
        jobtitle: "Senior Software Engineer",
        timeline: "2019 -  2024",
        desc: "Developed scalable framework for non-engineers to drag and drop components in Unity Editor to assemble content building template. Utilize custom template to generate the data field, and create a data-driven end-user content. This increased productivity by 200%.",
        techstacks: ["Unity3D", "C#", "CI/CD"]
    },
    {
        company: "Age of Learning",
        jobtitle: "Software Engineer III",
        timeline: "2015 -  2019",
        desc: "Developed a data-driven templates and webforms associate with template to create end-user content. This increased productivity by 150%.",
        techstacks: ["Unity3D", "C#"]
    },
    {
        company: "Age of Learning",
        jobtitle: "Software Engineer II",
        timeline: "2012 -  2015",
        desc: "Developed Over 300 mini-games for ABCMouse.com using Vanilla JavaScript/HTML5/CSS. ",
        techstacks: ["JavaScript", "HTML5", "CSS","PHP","MySQL"]
    },
    {
        company: "Age of Learning",
        jobtitle: "Software Engineer",
        timeline: "2010 -  2012",
        desc: "Developed Over 100 mini-games for ABCMouse.com using Flash/ActionScript.",
        techstacks: ["Flash", "ActionScript"]
    }
];
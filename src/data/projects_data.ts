import SudokuImg from '../assets/img-sudoku.gif'

export interface ILinks {
    name: string;
    link: string;
}

export interface IProjectsData {
    title: string;
    image: string;
    desc: string;
    techstacks: string[];
    links: ILinks[];
}


export const ProjectsData:IProjectsData[] = [
    {
        title: "My Portfolio Website", 
        image: null,
        desc: "This very website.\nSeparate the data and components for ease of update contents of each sections.\nGitlab CI/CD Setup to upload the latest commit on main to AWS S3 for static web hosting.",
        techstacks: ["React.js","TypeScript", "Vite", "Gitlab CI"],
        links: [
            {
                name: "Gitlab Repo",
                link: "https://gitlab.com/ctpupu/portfolio-website"
            }
        ]
    },
    {
        title: "Image Sudoku", 
        image: SudokuImg,
        desc: "Similar to sudoku game. Instead of using numbers, it uses images. Also offers different types of board with irragular shape or group.\nMade with Unity with C#.",
        techstacks: ["Unity", "UI Toolkit", "C#"],
        links: [
            {
                name: "Live Demo",
                link: "http://image-sudoku.s3-website-us-west-2.amazonaws.com/"    
            },
            {
                name: "Gitlab Repo",
                link: "https://gitlab.com/ctpupu/image-sudoku"
            }
        ]
    },
    {
        title: ".NET Web API Sample",
        image: null,
        desc: "Basic CRUD operations using web API.\nBasic Error Handling and Service Dependency Injection.",
        techstacks: [".NET6", "Web API"],
        links: [
            {
                name: "Gitlab Repo",
                link: "https://gitlab.com/ctpupu/dotnet-web-api-sample"
            }
        ]
    }
]